package ru.brd.Remote_Camera;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.net.Socket;

public class CamCliActivity extends Activity implements View.OnClickListener{

    String Ip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.cli);

        findViewById(R.id.btnMakePhoto).setOnClickListener(this);

        Intent intent = getIntent();
        Ip = intent.getStringExtra("Ip");

    }

    @Override
    public void onClick(View v) {

        RecvImage r = new RecvImage();
        r.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


    class RecvImage extends AsyncTask<String, String, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {

            Socket socket;
            DataInputStream inputS;
            try {

                socket = new Socket(Ip, 55000);
                inputS = new DataInputStream(socket.getInputStream());
                int size = inputS.readInt();
                byte[] buffer = new byte[size];

                int readed = 0;

                while (readed < size)
                    readed += inputS.read(buffer, readed, size-readed);

                inputS.close();
                socket.close();

                Bitmap bmp = BitmapFactory.decodeByteArray(buffer, 0, buffer.length );
                return bmp;

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bmp) {
            if(bmp != null)
                ((ImageView)findViewById(R.id.imageView)).setImageBitmap(bmp);
            else
                Toast.makeText(getBaseContext(), "ATATA :(", Toast.LENGTH_SHORT).show();
        }
    }

}
