package ru.brd.Remote_Camera;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ConnectCamServ extends Activity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connect);

        ((Button)findViewById(R.id.btnConnect)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String ip = ((EditText)findViewById(R.id.etIp)).getText().toString();
        Intent intent = new Intent(getBaseContext(),CamCliActivity.class);
        intent.putExtra("Ip", ip);
        startActivity(intent);
    }

}
