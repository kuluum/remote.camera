package ru.brd.Remote_Camera;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by zen on 06.11.14.
 */
public class StartActivity extends Activity implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);
        ((ImageButton)findViewById(R.id.btnCamServ)).setOnClickListener(this);
        ((ImageButton)findViewById(R.id.btnCamCli)).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCamServ:
                Intent intent = new Intent(this, CamServActivity.class);
                startActivity(intent);
                break;
            case R.id.btnCamCli:
                intent = new Intent(this, ConnectCamServ.class);
                startActivity(intent);

        }
    }
}
